# 清池app前端项目

### 介绍
清池app前端项目，日活3000已上线

### 注意
因为涉及到第三方sdk，未上传mainfest.json文件，请大家从其他项目复制

### 体验

app已上架，360、oppo、vivo、小米、阿里应用中心，大家可搜索 清池 自行下载。

也可点击下载链接 [下载app](https://openbox.mobilem.360.cn/index/d/sid/4534383)

已上架，微信小程序、qq小程序、可自行搜索 清池

#### Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```
<table>
  <thead>
  <tr>
    <th>微信扫码体验</th>
    <th>想要交流社交产品的朋友可以进群</th>
    <th>个人微信，有问题可以咨询</th>
  </tr>
  </thead>
  <tbody>
  <tr>
      <td align="center" valign="middle">
        <img width="222px" src="https://cdxapp-1257733245.cos.ap-beijing.myqcloud.com/qingchi/home/qingchiwxcode.jpg!thumbnail">
      </td>
      <td align="center" valign="middle">
        <img width="222px" src="https://cdxapp-1257733245.cos.ap-beijing.myqcloud.com/qingchi/static/qqgroupcode.png">
      </td>
      <td align="center" valign="middle">
        <img width="222px" src="https://cdxapp-1257733245.cos.ap-beijing.myqcloud.com/qingchi/static/wxcode.png">
      </td>
    </tr>
  <tr></tr>
  </tbody>
</table>
