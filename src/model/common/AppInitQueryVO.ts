export default class AppInitQueryVO {
  //上次退出时记录的adCode
  public adCode: string
  public homeType: string
  public gender: string
  public minAge: number
  public maxAge: number
  public lon: number
  public lat: number
  public openPosition: boolean
}
