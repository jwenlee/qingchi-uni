export default class LoadMoreType {
    // loading前
    static readonly more: string = 'more'
    // loading中
    static readonly loading: string = 'loading'
    // 没有更多了
    static readonly noMore: string = 'noMore'
}
